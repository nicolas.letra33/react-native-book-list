import * as React from 'react';
import Navigation from './components/Navigation.js';

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    return (
      <Navigation />
    );
  }  
}