# React Native - Book List

## I - Présentation

Cette petite application mobile permet de faire une recherche par auteur et d'obtenir la liste des livres écrit par cet auteur.  
Il est ensuite possible en cliquant sur un des livres de la liste d'accéder à la page de détail du livre en question qui contient également un lien vers l'achat dans le playstore.  
Cette application utilise l'API Google Books pour aller chercher les différents informations sur les livres et les auteurs.  

## II - Prérequis

Node.js  
expo-cli `npm install -g expo-cli`  
L'application expo sur votre smartphone  

## III - Lancement du projet

Installer les dépendances du projet :  
`npm install`  
Se placer à la racine du projet et lancer la commande :  
`expo start`  
Un serveur local est alors lancé et un QR code est généré, il faut ensuite démarrer l'application expo de votre smartphone et scanner ce QR code.  
Une fois scanné l'application se lance sur votre smartphone.  
Il est recommandé d'utiliser la même connexion pour l'ordinateur qui lance le serveur de dev et pour le smartphone.
