import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import BookList from './BookList.js'
import Bibliography from './Bibliography.js';
import Detail from './Detail.js';

export default class Navigation extends React.Component {
    constructor(props) {
      super(props);
    }
  
    render() {
      const Stack = createNativeStackNavigator();
  
      return (
       <NavigationContainer>
        <Stack.Navigator initialRouteName="BookList">
          <Stack.Screen name="BookList" component={BookList} />
          <Stack.Screen name="Bibliographie" component={Bibliography} />
          <Stack.Screen name="Detail" component={Detail} />
        </Stack.Navigator>
      </NavigationContainer>
      );
    }
  }