import { StyleSheet, Text, View, ActivityIndicator, Image, Linking, Button, TextInput} from 'react-native';
import * as React from 'react';

export default class AuthorName extends React.Component {
    constructor(props) {
      super(props);
    }


    render() {

      return (
        <View style={styles.container}>
            <Text style={styles.author_name}>{this.props.data}</Text>
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingBottom: 20,
    },
    author_name:{
      textAlign:'center',
      fontWeight:'bold',
      fontSize: 22,
      padding: 10,
      backgroundColor: '#EEEEEE',
      borderRadius: 20,
    }
  });
  