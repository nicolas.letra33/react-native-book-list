import { StyleSheet, Text, View, ActivityIndicator, Image, Linking, Button, TextInput} from 'react-native';
import * as React from 'react';

export default class BookList extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        author:'',
      };
    }

    concat(author){
        return 'https://www.googleapis.com/books/v1/volumes?q=inauthor:'+author
    }

    render() {
    const { author } = this.state;

      return (
        <View style={styles.container}>
            <Text style={styles.home_title}>Rechercher le nom d'un Auteur</Text>
            <TextInput
                style={styles.home_searchbar}
                onChangeText={(author) => this.setState({author})}
                value={author}
                placeholder = "ex: musso"
            />            
            <View style={styles.home_button}>
                <Button title="Rechercher"
                            color="#970000"
                            onPress={() => this.props.navigation.navigate('Bibliographie',{author:this.concat(author), author_name:author})}
                />
            </View>
            <View style={styles.home_img_container}>
                <Image style={styles.home_img}
                    source={{uri: 'https://assets.stickpng.com/images/580b585b2edbce24c47b2772.png',}}
                />
            </View>
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 40
    },
    home_title:{
      textAlign:'center',
      fontWeight:'bold',
      fontSize: 18,
    },
    home_searchbar:{
        height: 40,
        margin: 20,
        borderWidth: 1,
        padding: 10,
    },
    home_button:{
        paddingRight: 50,
        paddingLeft: 50
    },
    home_img_container:{
        flex: 1,
        alignItems:'center',
        marginTop: 50
    },
    home_img:{
        width: 250,
        height: 250,
    }
  });
  