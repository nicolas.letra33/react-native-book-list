import { StyleSheet, Text, View, ActivityIndicator, Image, Linking, Button, TextInput} from 'react-native';
import * as React from 'react';

export default class EmptyList extends React.Component {
    constructor(props) {
      super(props);
    }


    render() {

      return (
        <View style={styles.container}>
            <Text style={styles.empty_message}>Aucun résultat pour cet auteur</Text>
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingBottom: 20,
    },
    empty_message:{
      textAlign:'center',
      fontSize: 18,
      padding: 10,
    }
  });
  