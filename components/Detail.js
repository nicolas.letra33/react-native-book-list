import { StyleSheet, Text, View, ActivityIndicator, Image, Linking, Button} from 'react-native';
import * as React from 'react';

export default class Detail extends React.Component{

    constructor(props) {
        super(props);
    
        this.state = {
          data: {type: Object},
          isLoading: true,
        };
      }
    
      async getBook() {
        try {
          const response = await fetch( this.props.route.params.book );
          const json = await response.json();
          this.setState({ data: json });
        } catch (error) {
          console.log(error);
        } finally {
          this.setState({ isLoading: false });
        }
      }
    
      truncateDescription(text){
        if (text){
          var sub = text.toString();
          return `${sub.substring(0, 200)}...`;
          }
        return "";
      }

      cleanHTML(str){
        if ((str===null) || (str===''))
        {
        return false;
        }
        else
        {
        str = str.toString();
        return str.replace(/<[^>]*>/g, '');
        }
      }

      checkIfAvailable(str, str2){
        if(str){
          return false;
        }
        else{
          if(str2){
            return false;
          }
          return true;
        }
      }
    
      componentDidMount() {
        this.getBook();
      }
    
      render() {
        const { data, isLoading } = this.state;
    
        return (
          <View style={styles.container}>
            {isLoading ? <ActivityIndicator/> : (
                  <Text style={styles.book} onPress={() => this.props.navigation.navigate('Detail')}>
                    <Text style={styles.book_title}>{data.volumeInfo.title}, { data.volumeInfo.publishedDate }{"\n"}</Text>
                    <Text>{"\n"}{this.cleanHTML(this.truncateDescription(data.volumeInfo.description))}{"\n"}{"\n"}</Text>
                    <View><Image style={styles.book_img}
                      source={{
                        uri: data.volumeInfo.imageLinks
                          ? data.volumeInfo.imageLinks.smallThumbnail
                          : "https://www.pixenli.com/image/ixkvEvZ4",
                      }}
                     /></View><Text>{"\n"}</Text>
                    <View style={styles.button_pos}>
                    <Button title="Acheter"
                            color="#970000"
                            disabled={this.checkIfAvailable(data.saleInfo.buyLink, data.buyLink)}
                            onPress={() => Linking.openURL(data.saleInfo.buyLink ? data.saleInfo.buyLink : data.buyLink)} />
                    </View>
                  </Text>
            )}
          </View>
        );
      }  
    }
    
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        backgroundColor: '#B9B9B9',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
      }, 
      book:{
        backgroundColor: '#EEEEEE',
        marginBottom:20,
        padding: 10,
        textAlign:'center',
        borderRadius: 20,
      },
      book_title:{
        fontWeight:'bold',
        fontSize: 18,
      },
      book_img:{
        width: 160,
        height: 240
      },
      button_pos:{
        padding:10
      }
    });