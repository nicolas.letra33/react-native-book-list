import { StyleSheet, Text, View, ActivityIndicator, FlatList, Image} from 'react-native';
import * as React from 'react';
import AuthorName from './AuthorName.js';
import EmptyList from './EmptyList.js';

export default class Bibliography extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: true
    };
  }

  async getBooks() {
    try {
      const response = await fetch( this.props.route.params.author );
      const json = await response.json();
      this.setState({ data: json.items });
    } catch (error) {
      console.log(error);
    } finally {
      this.setState({ isLoading: false });
    }
  }

  componentDidMount() {
    this.getBooks();
  }

  render() {
    const { data, isLoading } = this.state;

    return (
      <View style={styles.container}>
        {isLoading ? <ActivityIndicator/> : (
          <FlatList
          style={styles.book_list}
           ListHeaderComponent={<AuthorName data={this.props.route.params.author_name} />}
           ListEmptyComponent={<EmptyList/>}
            data={data}
            keyExtractor={({ id }, index) => id}
            renderItem={({ item }) => (
              <Text style={styles.book} onPress={() => this.props.navigation.navigate('Detail',{book:item.selfLink})}>
                <Text style={styles.book_title}>{item.volumeInfo.title}{"\n"}{"\n"}</Text>
                <View><Image style={styles.book_img}
                  source={{
                    uri: item.volumeInfo.imageLinks
                      ? item.volumeInfo.imageLinks.smallThumbnail
                      : "https://www.pixenli.com/image/ixkvEvZ4",
                  }}
                /></View>{"\n"}
              </Text>
            )}
          />
        )}
      </View>
    );
  }  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  book_list:{
    backgroundColor: '#B9B9B9',
    padding: 20,
  },
  book:{
    backgroundColor: '#EEEEEE',
    borderRadius: 20,
    marginBottom:20,
    padding: 20,
    textAlign:'center',
  },
  book_title:{
    fontWeight:'bold',
    fontSize: 18,
  },
  book_img:{
    width: 160,
    height: 240
  }
});
